# homepage

> Future Coders

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

# memo
- IE11でcardのimgが縦に伸びる→cardにd-blockクラスを追加
- ページ遷移でstoreがクリアされる→btn.hrefではなくnuxt-linkで遷移
- store.samplesのgetterが呼ばれない→とりあえずstoreを直接呼び出すことで回避→呼ばれていたがタイミングがNG
- _id.vueで試していたらcreatedはサーバ側で実行されること、クライアント側で実行されること両方あるっぽい
- localStorageに退避→getter経由でアクセスすることで回避(computedから実行すれば大丈夫、createdでは早すぎる)
- nextServerInitがstoreのオブジェクトで呼ばれない

- vue2-google-maps.js IE11で動かない→no-ssrでscript要素を使って読み込み
- persistentはIE11で不具合あるので断念→RESTFULへ変更
- b-paginationがIE11で動かないと思たt→googlemapが原因だったっぽい、単体だとIE11でも動作
- aframe.ioを外してもLightHouseのPeformanceは42点止まり、blog/sample/bookを外しても30点台後半
- npm run generateを実行すれば、サーバ側でのgenerateをローカルで検証できる。sitemap.xml作成時に有効

# deployの方法
bitbucketへアップロード
- kenichiro@future-coders.netアカウントでbitbucketへログイン
- git remote add origin https://future-coders@bitbucket.org/future-coders/future-coders-hp.git
- git push -u origin master
netlifyと連携
- build command: npm run generate
- publish directory: dist
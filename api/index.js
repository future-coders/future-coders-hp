const express = require("express");
const request = require('request');
const app = express();

app.get("/email/:email/name/:name/age/:age/option/:option/comment/:comment", function (req, res) {

  var data = {
    personalizations: [{
      to: [{
        email: req.params['email'],
        name: req.params['name']
      },{
        email: "info@future-coders.net",
        name: "Future Coders"
      }],
      subject: "お問合せありがとうございます"
    }],
    content: [{
      type: "text/html",
      value: req.params['name'] + 
        '様<br/>お問合せありがとうございます。<br/>後ほど ' + req.params['email'] + ' へご連絡させていただきます。' +
        '<ul>' + 
        '<li>ご年齢：' + req.params['age'] + '</li>' +
        '<li>お問合せ：' + req.params['option'] + '</li>' +
        '<li>コメント' + req.params['comment'] + '</li>' +
        '</ul>'
        
    }],
    from: {
      email: "info@future-coders.net",
      name: "Future Coders"
    }
  }

  request.post({
    url: 'https://api.sendgrid.com/v3/mail/send',
    headers: {
      "Content-Type": "application/json",
      "authorization": "Bearer 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"
    },
    body: JSON.stringify(data)
  }, function (err, repsonse, body) {
    console.log(body)
  })
  res.send('hello world');
});

module.exports = {
  path: "/send/",
  handler: app
};

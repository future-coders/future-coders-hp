const pkg = require('./package')
const config = require('./.contentful.json')
const contentful = require('contentful');
const client = contentful.createClient({
  space: config.CTF_SPACE_ID,
  accessToken: config.CTF_CDA_ACCESS_TOKEN
});
const blogPromise = client.getEntries({
  'content_type': 'blog',
  order: '-sys.createdAt'
});
const samplePromise = client.getEntries({
  'content_type': 'sample',
  order: '-sys.createdAt'
});
module.exports = {
  mode: 'universal',
  //  mode : 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: '実践的プログラミングスクール | 無料体験',
    titleTemplate: 'Future Coders | %s',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: '元住吉・武蔵小杉の実践的プログラミングスクール'
      },
      {
        hid: 'og:site_name',
        property: 'og:site_name',
        content: 'Future Coders'
      },
      {
        hid: 'og:type',
        property: 'og:type',
        content: 'website'
      },
      {
        hid: 'og:url',
        property: 'og:url',
        content: 'https://www.future-coders.net'
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: 'Future Coders'
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: 'プログラミングスクール'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://www.future-coders.net/images/logo.png'
      }
    ],
    script: [{
      src: 'https://aframe.io/releases/0.9.2/aframe.min.js',
      defer: true
    }],
    link: [{
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        rel: 'stylesheet',
        href: 'https://unpkg.com/aos@next/dist/aos.css'
      },
      {
        rel: "preload",
        href: 'https://dpdb.webvr.rocks',
        as: 'script'
      }
    ],
    htmlAttrs: {
      prefix: 'og: http://ogp.me/ns#',
      lang: 'ja'
    },
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },

  /*
   ** Global CSS
   */
  css: [

  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [{
    src: '~/plugins/vue-lazyload.js',
    ssr: false
  }],

  /*
   ** Nuxt.js modules
   */
  modules: [
    ['@nuxtjs/google-analytics', {
      id: 'UA-91541923-1'
    }],
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    '@nuxtjs/markdownit',
    'nuxt-fontawesome',
    '@nuxtjs/sitemap',
    '@nuxtjs/pwa',
    // With options
    ['@nuxtjs/google-gtag', {
      id: 'UA-91541923-1',
    }]
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },

  env: {
    CTF_SPACE_ID: config.CTF_SPACE_ID,
    CTF_CDA_ACCESS_TOKEN: config.CTF_CDA_ACCESS_TOKEN,
    CTF_PERSON_ID: config.CTF_PERSON_ID,
    CTF_BLOG_POST_TYPE_ID: config.CTF_BLOG_POST_TYPE_ID
  },

  markdownit: {
    injected: true, // $mdを利用してmarkdownをhtmlにレンダリングする
    breaks: true, // 改行コードを<br>に変換する
    html: true, // HTML タグを有効にする
    linkify: true, // URLに似たテキストをリンクに自動変換する
    typography: true, // 言語に依存しないきれいな 置換 + 引用符 を有効にします。
  },

  fontawesome: {
    imports: [{
      set: '@fortawesome/free-solid-svg-icons',
      icons: ['fas']
    }]
  },

  sitemap: {
    path: '/sitemap.xml',
    hostname: 'https://future-coders.net',
    async routes() {
      return blogPromise.then(posts => {
        let urls = [];
        posts.items.forEach((val, idx, arr) => {
          urls[idx] = "https://www.future-coders.net/blog/" + val.sys.id;
        });
        return urls;
      })
    }
  },

  generate: {
    async routes() {
      return Promise.all([blogPromise, samplePromise])
        .then(([blogs, samples]) => {
          const r = []
          blogs.items.forEach(item => {
            r.push(`blog/${item.sys.id}`)
          })
          samples.items.forEach(item => {
            r.push(`sample/${item.sys.id}`)
          })
          return r;
        })
    }
  },
}
